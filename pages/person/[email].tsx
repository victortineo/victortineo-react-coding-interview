import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import { Button, PageHeader, Descriptions, Input, message, DatePicker } from 'antd';
import moment from 'moment'

import { withContextInitialized } from '../../components/hoc';
import CompanyCard from '../../components/molecules/CompanyCard';
import GenericList from '../../components/organisms/GenericList';
import OverlaySpinner from '../../components/molecules/OverlaySpinner';
import { usePersonInformation } from '../../components/hooks/usePersonInformation';

import { Company } from '../../constants/types';
import { ResponsiveListCard } from '../../constants';

const PersonDetail = () => {
  const router = useRouter();
  const [isEditting, setEditting] = useState(false)
  const [isUpdated, setUpdated] = useState(false)
  const [editName, setEditName] = useState("")
  const [editPhone, setEditPhone] = useState("")
  const [editGender, setEditGender] = useState("")
  const [editBirthday, setEditBirthday] = useState("")
  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );

  const handleEditSubmit = () => {
    if(isEditting) {
      save({
        ...data,
        gender: editGender,
        name: editName,
        phone: editPhone,
        birthDay: editBirthday
      })
      return setEditting(false)
    }
    setEditting(true)
  }

  useEffect(() => {
    load()
  }, []);

  useEffect(() => {
    if(!loading && !isUpdated) {
      if(data) {
        setEditName(data.name)
        setEditPhone(data.phone)
        setEditGender(data.gender)
        setEditBirthday(data.birthday)
        
        setUpdated(true)
      }
    }
  }, [loading, data])

  if (loading) {
    return <OverlaySpinner title={`Loading ${router.query?.email} information`} />;
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push('/home')
    );
    return <></>;
  }

  return (
    <>
      <PageHeader
        onBack={router.back}
        title="Person"
        subTitle="Profile"
        extra={[
          <Button
            style={{ padding: 0, margin: 0 }}
            type="link"
            href={data.website}
            target="_blank"
            rel="noopener noreferrer"
          >
            Visit website
          </Button>,
          <Button type="default" onClick={() => handleEditSubmit()}>
            {isEditting ? 'Submit' : 'Edit'}
          </Button>,
        ]}
      >
        {data && (
          isEditting ? (
              <>
                <Input placeholder="Name" value={editName} onChange={(e) => setEditName(e.target.value)} />
                <Input placeholder="Gender" value={editGender} onChange={(e) => setEditGender(e.target.value)} />
                <Input placeholder="Phone" value={editPhone} onChange={(e) => setEditPhone(e.target.value)} />
                <DatePicker placeholder="Birthday" value={moment(editBirthday)} onChange={(e) => setEditBirthday(e.toString())} />
              </>
            ) : 
              <Descriptions size="small" column={1}>
                <Descriptions.Item label="Name">{data.name}</Descriptions.Item>
                <Descriptions.Item label="Gender">{data.gender}</Descriptions.Item>
                <Descriptions.Item label="Phone">{data.phone}</Descriptions.Item>

                <Descriptions.Item label="Birthday">{data.birthday}</Descriptions.Item>
              </Descriptions>
        )}
        <GenericList<Company>
          loading={loading}
          extra={ResponsiveListCard}
          data={data && data.companyHistory}
          ItemRenderer={({ item }: any) => <CompanyCard item={item} />}
          handleLoadMore={() => {}}
          hasMore={false}
        />
      </PageHeader>
    </>
  );
};

export default withContextInitialized(PersonDetail);
